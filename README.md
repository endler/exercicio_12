# exercicio_12
Utilizando a API do cartola e o projeto pronto, execute as tarefas a seguir. 

Informações sobre a API do cartola:

- https://api.cartolafc.globo.com/partidas
Traz as partidas da próxima rodada, além da lista de clubes e suas respectivas posições na tabela do Campeonato Brasileiro 2019 Série A.

- https://api.cartolafc.globo.com/partidas/<rodada>
Traz os resultados das partidas da rodada especificada. Também traz a posição daquele clube ao início dessa rodada.

- https://api.cartolafc.globo.com/atletas/mercado
Traz a lista de atletas de todos os clubes da Série A do Campeonato Brasileiro 2019.

# Utilizar BLoC 

## Tarefa 01

Utilizar o padrão BLOC para pegar a lista de clubes e exibir em uma ListView. 
OBS: O método que baixa o JSON e converte em uma lista de objetos “Clube” já está implementado

## Tarefa 02

Implementar uma lista com as partidas e o placar do jogo, de uma rodada escolhida pelo usuário. (Rodadas 1-5)


# Utilizar MobX 

## Tarefa 1

Faça o acesso a API do cartola para buscar a lista de clubes e exiba em uma ListView. Obs.: O método para fazer a requisição já está implementado. 
Use o padrão MobX para observar a variável que contém a lista de clubes e execute a alteração na tela no momento em que você tiver a resposta async da API. 

## Tarefa 2

Utilizando o mesmo controller anterior, implemente o padrão de Observable para uma variável que contém a rodada sendo mostrada. Quando o usuário escolher alterar essa rodada, execute uma nova ação para requisitar os resultados dessa rodada, via API. 