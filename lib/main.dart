import 'package:exercicio_12/resources/clube_api_provider.dart';
import 'package:flutter/material.dart';

import 'model/clube.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter BLOC',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Flutter BLOC'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Clube> lista;
  ClubeApiProvider c = ClubeApiProvider();

  @override
  initState() {
    super.initState();
    getClubes();
  }

  void getClubes() async {
    lista = await c.fetchClubes();
    // lista.sort((a, b) {
    //   return a.posicao.compareTo(b.posicao);
    // });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemCount: lista.length,
        itemBuilder: (BuildContext context, int index) {
          Clube a = lista[index];
          return Card(
            child: ListTile(
              title: Text(a.nome),
              leading: Image.network(a.img_escudo),
            ),
          );
        },
      ),
    );
  }
}
