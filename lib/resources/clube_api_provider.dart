import 'package:exercicio_12/model/clube.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class ClubeApiProvider {
  Future<List<Clube>> fetchClubes() async {
    http.Response response =
        await http.get('https://api.cartolafc.globo.com/partidas');
    Map clubes = jsonDecode(response.body)['clubes'];
    print(clubes.values.toList());
    return clubes.values.toList().map((m) => Clube.fromJson(m)).toList();
  }
}
