class Clube {
  int id;
  String nome;
  String abreviacao;
  //int posicao;
  String img_escudo;

  Clube.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        nome = json['nome'],
        abreviacao = json['abreviacao'],
        //posicao = json['posicao'],
        img_escudo = json['escudos']['45x45'];
}
